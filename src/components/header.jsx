import React from "react";

function header(props){

    return(

        <header className='head border-b'>
        <div className="container flex flex-row justify-between item-center mx-auto py-5">
          <div className="logo">
          <span id="logo" className='logo text-xl font-bold'>ShoppingZone</span> 
          </div>
      
      
         <div className="list link">
          <ul className='list flex flex-row gap-10 text-gray-800'>
            <li><a href="#">Home</a></li>
            <li><a href="#">Products</a></li>
            <li><a href="#">Categories</a></li>
            <li><a href="#">About</a></li>
          </ul>
        </div>
      
      
        </div>
      
      </header>

    );
}
export default header;