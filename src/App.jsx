import Header from './components/header'
import './App.css'

function App() {


  return (
    <>
    < Header />

<section id='s1' className="s1 bg-center bg-cover bg-[url('./pexels-arina-krasnikova-5418888.jpg')] flex flex-row justify-center item-center text-white">

  <div className="container flex flex-col justify-center item-center ">
    <h1 className='h1 text-5xl font-bold'>"Welcome to ShoppingZone<br></br>&nbsp; Your Ultimate Online Shopping Destination!"</h1>
    
  </div>

</section>






    <section id='s2' className="s2  mt-7 ">
         <div className="maincontainer flex flex-row justify-center item-center">
             <div id='productpicture' className="productpicture flex flex-row item-center justify-center">
                 
             </div>
             <div id='productdetails' className="productdetails flex flex-col ">
                   <span id="title">Title</span>
                   <span id="productcategory">Category</span>
                   <span id="productprice">Price</span>
                   <span id="productdescription">Description</span>
                <div className="btn flex flex-row gap-4">
                   <button className='primary bg-black p-3 px-10 text-white'>Buy Now</button>
                   <button className='secondary border-2 border-gray-300 p-3 px-10'>Add To Cart</button>
                </div>
                   
             </div>
         </div>
    </section>

<section id="s3" className='s3 m-3'>
  <div id='productlist' className="productlist">
   
    
    
    </div>
</section>
    </>
  )
}

export default App
